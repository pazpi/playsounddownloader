use anyhow::{anyhow, Context, Result};
use clap::Parser;
use reqwest::Client;

use crate::programmi_json::Programmi;

mod audio_tags;
mod http;
mod programmi_json;
mod cli_args;

const SITE_URL: &str = "https://www.raiplaysound.it";

#[tokio::main]
async fn main() -> Result<()> {
    let args: cli_args::AppArgs = cli_args::AppArgs::parse();

    let url = http::verify_url(args.url)?;

    let http_client = &Client::new();

    let programmi: Programmi = http::parse_json(http_client, &url)
        .await
        .context("Impossibile ottenere la lista dei programmi")?;

    println!("Programma trovato: {}", programmi.title);

    for card in &programmi.block.cards {
        println!(
            "Downloading \"{}\" -> {}",
            card.episode_title, card.downloadable_audio.url
        );

        http::get_episode(http_client, card)
            .await
            .and_then(|f| audio_tags::populate_tags(card, f.as_path()))
            .map_err(|e| anyhow!("Errore! {}", e))?;
    }

    Ok(())
}
