use std::cmp::min;
use std::fs::File;
use std::io::{Seek, Write};
use std::path::{Path, PathBuf};

use anyhow::{anyhow, Context, Result};
use futures_util::StreamExt;
use indicatif::{ProgressBar, ProgressStyle};
use reqwest::Client;
use url::Url;

use crate::programmi_json::{EpisodioCard, Programmi};
use crate::SITE_URL;

pub async fn download_file_progressbar(client: &Client, url: &str, path: &Path) -> Result<()> {
    let res = client.get(url).send().await?;

    let total_size = res
        .content_length()
        .context(format!("Impossibile ottenere la lunghezza del contenuto per l'url '{}'", &url))?;

    let pb = ProgressBar::new(total_size);
    pb.set_style(ProgressStyle::default_bar()
        .template("{msg}\n{spinner:.green} [{elapsed_precise}] [{wide_bar:.white/blue}] {bytes}/{total_bytes} ({bytes_per_sec}, {eta})")
        .progress_chars("█  "));

    let mut file;
    let mut downloaded: u64 = 0;
    let mut stream = res.bytes_stream();

    if path.exists() {
        println!("File esistente, riprendo il download");

        file = std::fs::OpenOptions::new()
            .read(true)
            .append(true)
            .open(path)
            .unwrap();

        let file_size = std::fs::metadata(path).unwrap().len();

        // File already downloaded
        if total_size <= file_size {
            return Ok(());
        }

        file.seek(std::io::SeekFrom::Start(file_size)).unwrap();
        downloaded = file_size;
    } else {
        file = File::create(path)?
    }

    while let Some(item) = stream.next().await {
        let chunk = item?;
        file.write_all(&chunk)?;
        let new = min(downloaded + (chunk.len() as u64), total_size);
        downloaded = new;
        pb.set_position(new);
    }

    pb.finish_with_message(&format!("Scaricato {} in {}", url, path.display()));
    Ok(())
}

async fn download_file(client: &Client, url: &str, path: &Path) -> Result<()> {
    let res = client.get(url).send().await?;

    let image = res.bytes().await?;

    return match File::create(path).and_then(|mut f| f.write_all(&image)) {
        Ok(_) => Ok(()),
        Err(e) => Err(anyhow!(
            "Impossibile salvare la copertina al percorso: \"{}\"\n\n{}",
            path.display(),
            e
        )),
    };
}

pub async fn get_episode(client: &Client, episode_info: &EpisodioCard) -> Result<PathBuf> {
    const BASE_DIR: &str = "audio";

    let dest_path = Path::new(".")
        .join(BASE_DIR)
        .join(&episode_info.subtitle)
        .join(format!("S{}", &episode_info.track_info.season));

    let file_name = format!("{}.mp3", &episode_info.episode_title);
    let audio_dest_path = dest_path.join(file_name);
    let cover_dest_path = dest_path.join("cover.jpg");

    std::fs::create_dir_all(&dest_path)?;

    let cover_url = format!("{}{}", SITE_URL, &episode_info.image);
    download_file(client, &cover_url, cover_dest_path.as_path())
        .await
        .unwrap_or(());

    return match download_file_progressbar(
        client,
        &episode_info.downloadable_audio.url,
        audio_dest_path.as_path(),
    )
        .await
    {
        Ok(_) => Ok(audio_dest_path),
        Err(e) => Err(anyhow!(format!(
            "Errore download file {}\n\n{}",
            &episode_info.downloadable_audio.url, e
        ))),
    };
}

pub async fn parse_json(client: &Client, url: &str) -> Result<Programmi> {
    let resp = client
        .get(url)
        .send()
        .await
        .context(format!("Errore richiesta: {}", url))?;

    let resp_str = resp
        .text()
        .await
        .context(format!("Errore estrazione testo richiesta: {}", url))?;

    let programmi: Programmi = serde_json::from_str(&resp_str).context("Errore parsing JSON")?;

    Ok(programmi)
}

pub fn verify_url(input: String) -> Result<String> {
    let in_url = match Url::parse(&input) {
        Ok(url) => url,
        Err(e) => return Err(anyhow!("URL non valido -> {}", e)),
    };

    let json_url = match in_url.path().ends_with(".json") {
        true => input,
        false => format!("{}.json", input),
    };

    Ok(json_url)
}
