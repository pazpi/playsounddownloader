use serde::Deserialize;
use serde_aux::prelude::*;

#[derive(Deserialize)]
pub struct DownloadInfo {
    pub url: String,
}

#[derive(Deserialize)]
pub struct TrackInfo {
    pub id: String,
    pub editor: String,
    pub title: String,
    pub program_title: String,

    pub genres: Vec<String>,
    pub sub_genres: Vec<String>,

    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub year: i32,

    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub season: u64,

    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub episode_number: u64,
}

#[derive(Deserialize)]
pub struct EpisodioCard {
    pub episode_title: String,
    pub subtitle: String,
    pub description: String,
    pub downloadable_audio: DownloadInfo,
    pub image: String,
    pub track_info: TrackInfo,
}

#[derive(Deserialize)]
pub struct EpisodioBlock {
    pub cards: Vec<EpisodioCard>,
}

#[derive(Deserialize)]
pub struct PodcastInfo {
    pub title: String,
    pub description: String,
    pub image: String,
}

#[derive(Deserialize)]
pub struct Programmi {
    pub title: String,
    pub block: EpisodioBlock,
    pub podcast_info: PodcastInfo,
}
