use std::io::Cursor;
use std::path::Path;

use anyhow::{anyhow, Result};
use id3::{Error, ErrorKind, Tag, TagLike, Version};

use crate::programmi_json::EpisodioCard;

pub fn populate_tags(episode_card: &EpisodioCard, audio_path: &Path) -> Result<()> {
    let mut tag = match Tag::read_from_path(audio_path) {
        Ok(tag) => tag,
        Err(Error {
                kind: ErrorKind::NoTag,
                ..
            }) => Tag::new(),
        Err(err) => return Err(anyhow!(err)),
    };

    tag.set_title(&episode_card.episode_title);
    tag.set_artist(&episode_card.track_info.editor);
    tag.set_album(&episode_card.track_info.program_title);
    tag.set_year(episode_card.track_info.year);

    if let Some(g) = episode_card.track_info.genres.first() {
        tag.set_genre(g);
    };

    if let Some(p) = audio_path.parent() {
        embed_image(&mut tag, Path::new(p.join("cover.jpg").as_path())).unwrap_or(());
    }

    tag.write_to_path(audio_path, Version::Id3v24)?;
    Ok(())
}

/// Embed the image from `image_filename` into `music_filename`, in-place. Any errors reading ID3
/// tags from the music file or parsing the image get propagated upwards.
///
/// The image is encoded as a JPEG with a 90% quality setting, and embedded as a "Front cover".
/// Tags get written as ID3v2.3.
///
pub fn embed_image(tag: &mut Tag, image_filename: &Path) -> Result<()> {
    let image = image::open(&image_filename)
        .map_err(|e| anyhow!("Errore apertura immagine {:?}: {}", image_filename, e))?;

    let mut encoded_image_bytes = Vec::new();
    image
        .write_to(
            &mut Cursor::new(&mut encoded_image_bytes),
            image::ImageOutputFormat::Jpeg(90),
        )
        .map_err(|e| {
            anyhow!(
                "Errore nella compressione JPEG {:?}: {}",
                image_filename,
                e
            )
        })?;

    tag.add_frame(id3::frame::Picture {
        mime_type: "image/jpeg".to_string(),
        picture_type: id3::frame::PictureType::CoverFront,
        description: String::new(),
        data: encoded_image_bytes,
    });

    Ok(())
}
