use clap::Parser;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct AppArgs {
    #[clap(value_parser)]
    pub url: String,

    #[clap(long, short, value_parser)]
    pub output: Option<String>,
}
